<!-- Navbar -->
<div class="w3-top">
    <div class="w3-bar w3-red w3-card w3-left-align w3-large">
        <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
        <a href="http://nabipour.xyz/" class="w3-bar-item w3-button w3-padding-large w3-white">صفحه نخست</a>
        <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"> تاریخچه php</a>
        <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"> تاریخچه html</a>
        <a href="register.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">ثبت نام</a>
        <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">درباره ما</a>
    </div>

    <!-- Navbar on small screens -->
    <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large">
        <a href="#" class="w3-bar-item w3-button w3-padding-large">تاریخچه php</a>
        <a href="#" class="w3-bar-item w3-button w3-padding-large"> تاریخچه html</a>
        <a href="register.php" class="w3-bar-item w3-button w3-padding-large">ثبت نام</a>
        <a href="#" class="w3-bar-item w3-button w3-padding-large">درباره ما</a>
    </div>
</div>

<!-- Header -->
<header class="w3-container  w3-red w3-center" style="padding:128px 16px">
    <h1 class="w3-margin w3-jumbo">طراحی سایت</h1>
    <p class="w3-xlarge">php & html</p>
    <button class="w3-button w3-black w3-padding-large w3-large w3-margin-top">Get Started</button>
</header>